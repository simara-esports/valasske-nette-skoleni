<?php

namespace Forms;

/**
 * Tovarna na jazykove casti formularu
 *
 * @author Svaťa
 */
class LangFormFactory extends \Nette\Object {
    
    /**
     * Creates container for languages
     * @return \Nette\Forms\Container
     */
    public function create() {
        $lang = new \Nette\Forms\Container;
        $lang->addSelect('id_lang', 'Jazyk', $this->getLangs());
        return $lang;
    }
    
    /**
     * Returns all available languages
     * @return array
     */
    protected function getLangs() {
        return array(1 => 'Čeština', 2 => 'Uzbečtina', 3 => 'Angličtina');
    }
}
