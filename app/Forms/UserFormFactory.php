<?php

namespace Forms;

/**
 * tovarna pro vytvareni uzivatelskeho formulare
 *
 * @author Svaťa
 */
class UserFormFactory extends \Nette\Object {
    
    /** @var callable */
    protected $linkCallback;
    
    /**
     * Vrati pripraveny formular pro pridavani uzivatele
     * @return \Nette\Application\UI\Form
     */
    public function create() {
        $form = new \Nette\Application\UI\Form;
        $renderer = new \Kdyby\BootstrapFormRenderer\BootstrapRenderer();        
        $form->setRenderer($renderer);
        //$linkCallback('Sign:in')
        $linkCallback = $this->linkCallback;
        $form->addText('email', 'Email')
                ->setOption('description', \Nette\Utils\Html::el("a","link")->href($linkCallback('Sign:in')));
        $form->addSubmit('save', 'Vytvorit');
        return $form;
    }
    
    public function setLinkCallback($linkCallback) {
        $this->linkCallback = $linkCallback;
    }


    
}
