<?php

namespace App\Presenters;

/**
 * Presenter pro praci s uzivatelama
 *
 * @author Svaťa
 */
class UserPresenter extends BasePresenter{

    /**
     * @var \Nette\Database\Context
     * @inject
     */
    public $db;
    
    /**
     * @var \Model\RegistrationCounter
     * @inject
     */
    public $registrationCounter;
    
    /**
     * @var \Model\UserFinder
     * @inject
     */
    public $userFinder;
    
    /**
     * @var \Model\UserRegistrator
     * @inject
     */
    public $userRegistrator;
    
    /**
     * @var \Forms\UserFormFactory
     */
    protected $userFormFactory;
    
    /**
     * @var \Components\User\IUserListFactory
     * @inject
     */
    public $userListFactory;
    
    public function injectUserFormFactory(\Forms\UserFormFactory $userFormFactory) {
        $this->userFormFactory = $userFormFactory;
        $this->userFormFactory->setLinkCallback($this->link);
    }
        
    public function renderList() {
        $this->template->users = $this->userFinder->find('sva');
        $this->template->registrationCounter = $this->registrationCounter;
    }
    
    public function actionRegister() {
        try{
            $this->template->newUser = $this->userRegistrator->register('Ja', 'Trovka', 'jabcd@ja.ja');
        }catch(\Model\UserDuplicateException $e){
            $this->flashMessage("Uživatel s tímto e-mailem ({$e->getUser()->email}) je již v systému zaregistrován.", 'error');
        }
    }
    
    public function createComponentUserForm() {
        $form = $this->userFormFactory->create();
        $form->onSuccess[] = $this->createUser;
        return $form;
    }
    
    public function createUser($form) {
        dump($form->getValues());
        //dump($form['address']->getValues());
        // create
        //$this->redirect('this');
    }
    
    public function createComponentUserList() {
        return $this->userListFactory->create('honda');
    }
    
}
