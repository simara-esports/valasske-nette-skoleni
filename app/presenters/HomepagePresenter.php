<?php

namespace App\Presenters;

use Nette,
	App\Model;


/**
 * Homepage presenter.
 */
class HomepagePresenter extends BasePresenter
{
    public function renderArticles() {
        $this->template->articles = $this->articleService->getLast(10);
    }
    
}
