<?php

namespace App\Presenters;

use Nette,
	App\Model;


/**
 * Base presenter for all application presenters.
 */
abstract class BasePresenter extends Nette\Application\UI\Presenter
{
    
    /**
     * @var \Esports\Templating\Helper\Loader
     * @inject
     */
    public $helperLoader;
    
    /**
     * @var \Components\ILoginFactory
     * @inject
     */
    public $loginFactory;
    
    protected function beforeRender() {
        parent::beforeRender();
        $this->template->registerHelperLoader($this->helperLoader);
    }
    
    // ------------------------- COMPONENTS -------------------------

    public function createComponentLogin() {
        return $this->loginFactory->create();
    }
    
    public function createComponentCss()
    {
            $compiler = $this->context->getService('webloader.cssDefaultCompiler');
            /** @var \WebLoader\Compiler $compiler */
            return new \WebLoader\Nette\CssLoader(
                $compiler,
                $this->template->basePath . $this->context->parameters['web']['tempDir']
            );
    }

    public function createComponentJs()
    {
            $compiler = $this->context->getService('webloader.jsDefaultCompiler');
            /** @var \WebLoader\Compiler $compiler */
            return new \WebLoader\Nette\JavaScriptLoader(
                $compiler,
                $this->template->basePath . $this->context->parameters['web']['tempDir']
            );
    }
}
