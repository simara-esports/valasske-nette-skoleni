<?php

namespace App\Presenters;

/**
 * Presenter pro clanky
 *
 * @author Svaťa
 */
class ArticlePresenter extends BasePresenter {
    
    /**
     * @var \Components\Article\IArticleFormFactory
     * @inject
     */
    public $articleFormFactory;
    
    public function renderEdit($id) {
        try{
            $this['editArticleForm']->loadDefaults();
        }catch(\InvalidArgumentException $e){
            $this->flashMessage('clanek neexistuje, pridej ho');
            $this->redirect('add');
        }
    }
    
    public function createComponentArticleForm() {
        $articleForm = $this->articleFormFactory->create();
        $articleForm['form']->onSuccess[] = $articleForm->insert;
        return $articleForm;
    }
    
    public function createComponentEditArticleForm() {
        $id = $this->getParameter('id');
        $articleForm = $this->articleFormFactory->create();
        $articleForm['form']->onSuccess[] = $articleForm->edit;
        $articleForm['form']['save']->caption = 'Editovat';
        $articleForm->setId($id);
        return $articleForm;
    }
    
}
