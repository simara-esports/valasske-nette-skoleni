<?php

namespace Components;

use Nette\Application\UI\Control;
use Nette\Application\UI\Form;
use Nette\Security\User;

/**
 * Login component
 *
 * @author Svaťa
 */
class Login extends Control {
    
    /** @var User */
    protected $user;
    
    function __construct(User $user) {
        parent::__construct();
        $this->user = $user;
    }
    
    public function createComponentLoginForm() {
        $form = new Form;
        $form->addText('login', 'Login');
        $form->addRadioList('type', 'Typ', 
                array(
                    'one' => 'Jedna',
                    'two' => 'Dva',
                    'tree' => 'Tri'
                    ));
        $form->addSubmit('doIt', 'Login');
        return $form;
    }
    
    public function render() {
        $this->template->setFile(__DIR__ . '/login.latte');
        $this->template->user = $this->user;
        $this->template->render();
    }
    
}

/** 
 * @autor Svaťa
 */
interface ILoginFactory {
   
    /**
     * @return Login
     */
    public function create();
    
}