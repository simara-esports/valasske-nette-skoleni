<?php

namespace Components\Article;

use Forms\LangFormFactory;
use Model\ArticleRepository;
use Kdyby\BootstrapFormRenderer\BootstrapRenderer;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;

/**
 * Formular pro pridavani clanku
 *
 * @author Svaťa
 */
class ArticleForm extends Control {
    
    /** @var \Forms\LangFormFactory */
    protected $langFormFactory;
    
    /** @var \Model\ArticleRepository */
    protected $articleRepository;
    
    /** @var int */
    protected $id;
    
    function __construct(LangFormFactory $langFormFactory, ArticleRepository $articleRepository) {
        parent::__construct();
        $this->langFormFactory = $langFormFactory;
        $this->articleRepository = $articleRepository;
    }

                
    public function createComponentForm() {
        $form = new Form;
        $form->setRenderer(new BootstrapRenderer);
        $form['lang'] = $this->langFormFactory->create();
        $form->addText('author', 'Autor');
        $form->addCheckbox('is_visible', 'Viditelny?')
                ->setDefaultValue(true);
        
        $form->addSubmit('save', 'Zpracovat');

        return $form;
    }
    
    public function render() {
        $this['form']->render();
    }
    
    /**
     * Normalizuje data z formulare pro pouziti v DB
     * @param array $data
     * @return array
     */
    protected function normalizeData($data) {
        $result = $data;
        $result['id_lang'] = $data['lang']['id_lang'];
        unset($result['lang']);
        return $result;
    }
    
    public function insert(Form $form) {
        $data = $this->normalizeData($form->getValues());
        $this->articleRepository->insert($data);
    }
    
    public function edit(Form $form) {
        $data = $this->normalizeData($form->getValues());
        $this->articleRepository->update($this->id, $data);
    }
    
    /**
     * Fills form by default values
     * @throws \InvalidArgumentException
     */
    public function loadDefaults() {
        $article = $this->articleRepository->get($this->id);
        if(!$article){
            throw new \InvalidArgumentException;
        }
        $this['form']->setDefaults($article);
        $this['form']['lang']['id_lang']
                ->setDefaultValue($article->id_lang);
    }
    
    // ------------------------- GETS & SETS -------------------------
    
    public function setId($id) {
        $this->id = $id;
    }
    
}

/**
 * Tovarna na formulare pro clanky
 */
interface IArticleFormFactory{
    
    /**
     * @return ArticleForm
     */
    public function create();
}