<?php

namespace Components\User;

use Esports\Templating\Helper\Loader;
use Model\UserRepository;
use Nette\Application\UI\Control;
use Nextras\Datagrid\Datagrid;

/**
 * Seznam uzivatelu
 *
 * @author Svaťa
 */
class UserList extends Control {
    
    /** @var UserRepository */
    protected $userRepository;
    
    /** @var Loader */
    protected $helperLoader;
    
    function __construct(Loader $helperLoader) {
        parent::__construct();
        $this->helperLoader = $helperLoader;
    }

    
    public function createComponentGrid() {
        $grid = new Datagrid();
        $grid->addColumn('name', 'Jmeno')->enableSort();
        $grid->addColumn('surname', 'Prijmeni');
        $grid->addColumn('email', 'E-mail');
        $grid->addColumn('country', 'Zeme');
        $grid->addColumn('test', 'Test');
        $grid->setDataSourceCallback($this->getData);
        $grid->addCellsTemplate(__DIR__ . '/userList.latte');
        $grid->template->registerHelperLoader($this->helperLoader);
        $grid->setPagination(10, $this->getDataCount);
        $grid->setFilterFormFactory($this->createFilterForm);
        
        return $grid;
    }
    
    public function createFilterForm() {
        $form = new \Nette\Forms\Container;
        $form->addText('name');
        $form->addSubmit('filter', 'Filtrovat');
        $form->addSubmit('cancel', 'Zrusit filtrovani');
        return $form;
    }
    
    public function getData($filter, $order, \Nette\Utils\Paginator $paginator = null) {
        $table = $this->userRepository->table();
        if($order){
            $orderString = implode(' ', $order);
            $table->order($orderString);
        }
        if($paginator){
            $table->page($paginator->page, $paginator->itemsPerPage);
        }
        if(isset($filter['name'])){
            $query = "%{$filter['name']}%";
            $table->where(
                    'name LIKE ? OR surname LIKE ? OR email LIKE ?',
                    $query, $query, $query);
        }
        return $table;
    }
    
    public function getDataCount($filter, $order) {
        return $this->getData($filter, $order)->count();
    }
    
    /**
     * Render grid of this component
     */
    public function render() {
        $this['grid']->render();
    }
    
    public function setUserRepository(UserRepository $userRepository) {
        $this->userRepository = $userRepository;
    }
    
    public function setName($name) {
        dump($name);
    }
}

/**
 * @autor Svaťa
 */
interface IUserListFactory {
    
    /**
     * @return UserList
     */
    public function create($name);
    
}