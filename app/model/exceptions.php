<?php

namespace Model;

/**
 * Uzivatel nebyl nalezen/neexistuje
 */
class UnknownUserException extends \Exception{
    
}

/**
 * Pokus o duplikaci nejakych dat
 */
class DuplicateException extends \Exception{
    
}

class UserDuplicateException extends DuplicateException{
    
    protected $user;
    
    function __construct($user) {
        parent::__construct();
        $this->user = $user;
    }
    
    public function getUser() {
        return $this->user;
    }
    
}