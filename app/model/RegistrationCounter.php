<?php

namespace Model;

/**
 * Pocitadlo registraci
 *
 * @author Svaťa
 */
class RegistrationCounter extends \Nette\Object {
    
    /**
     * Zjisti, kolik ma uzivatel registraci
     * @param \Nette\Database\Table\ActiveRow $user
     * @return int
     */
    public function registrationCount(\Nette\Database\Table\ActiveRow $user) {
        return $user->related('registration')->count();
    }
    
}
