<?php

namespace Model;

use \Nette\Database\Table\Selection;

/**
 * Sluzba, ktera vyhledava uzivatele
 *
 * @author Svaťa
 */
class UserFinder extends Repository {
    
    /**
     * hleda uzivatele podle dotazu
     * @param string $query
     * @return Selection
     */
    public function find($query) {
        $q = "%$query%";
        return $this->table()->where(
                'name LIKE ? OR surname LIKE ? OR email LIKE ?',
                $q, $q, $q);
    }
    
    /**
     * Hleda uzivatele podle emailu
     * @param string $email
     * @return ActiveRow
     * @throws UnknownUserException
     */
    public function findByEmail($email) {
        $user = $this->table()->where('email', $email)->fetch();
        if($user){
            return $user;
        }else{
            throw new UnknownUserException;
        }
    }
    
}
