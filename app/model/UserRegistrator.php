<?php

namespace Model;

use \Nette\Database\Table\Selection;

/**
 * Registrator uzivatelu
 *
 * @author Svaťa
 */
class UserRegistrator extends \Nette\Object {
    
    /** @var UserFinder */
    protected $userFinder;
    
    /** @var Selection */
    protected $userTable;
    
    function __construct(Selection $userTable, UserFinder $userFinder) {
        $this->userFinder = $userFinder;
        $this->userTable = $userTable;
    }

    /**
     * Zaregistruje uzivatele
     * @param string $name
     * @param string $surname
     * @param string $email
     * @return ActiveRow
     * @throws DuplicateException
     */
    public function register($name, $surname, $email) {
        try{
            $user = $this->userFinder->findByEmail($email);
            throw new UserDuplicateException($user);
        }catch(UnknownUserException $e){
            return $this->userTable->insert(
                array(
                    'name' => $name,
                    'surname' => $surname,
                    'email' => $email,
                )
            );
        }
    }
    
}
