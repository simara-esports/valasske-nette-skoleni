<?php

namespace Model;

use \Nette\Database\Table\Selection;
use \Nette\Database\Table\ActiveRow;

/**
 * Obhospodarovani jedne tabulky
 *
 * @author Svaťa
 */
abstract class Repository extends \Nette\Object {
    
    /** @var \Nette\Database\Table\Selection */
    private $table;
    
    function __construct(Selection $table) {
        $this->table = $table;
    }

    /**
     * Varti cistou tabulku
     * @return \Nette\Database\Table\Selection
     */
    public function table(){
        return clone $this->table;
    }
    
    /**
     * Vytvori novy zaznam
     * @param array $data
     * @return ActiveRow
     */
    public function insert($data){
        return $this->table()->insert($data);
    }
    
    /**
     * Updates a row in DB
     * @param int $id
     * @param array $data
     * @return bool
     */
    public function update($id, $data) {
        return $this->get($id)->update($data);
    }
    
    /**
     * Naklonuje radek z tabulky s ID
     * @param int|ActiveRow $id
     * @return ActiveRow
     */
    public function cloneRow($id){
        if($id instanceof ActiveRow){
            $id = $id->getPrimary();
        }
        return $this->get($id);
    }
    
    /**
     * Vraci radek z tabulky s ID
     * Pokud je parametrem ActiveRow, vraci jej v nezmenene podobe
     * @param int|ActiveRow $id
     * @return ActiveRow
     */
    public function get($id){
        if($id instanceof ActiveRow){
            return $id;
        }
        return $this->table()->get($id);
    }
    
}
