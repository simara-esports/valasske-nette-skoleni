<?php

namespace Tables;

/**
 * Sluzba nad tabulkou article_category
 *
 * @author Svaťa
 */
class ArticleCategoryTable extends Repository {

    /**
     * Vraci pouze viditelne 
     * @param int
     * @return Selection
     */
    public function getVisible($langId) {
        $table = $this->table();
        if($langId){
            $table->where('id_lang', $langId);
        }
        return $table->where('is_visible', true);
    }
}
