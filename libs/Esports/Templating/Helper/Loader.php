<?php

namespace Esports\Templating\Helper;

use \Nette\Object;

/**
 * Helper loader delegujici praci na registrovane tridy 
 */
class Loader extends Object{
    
    /**
     * 
     * @var array
     */
    protected $helpers = array();
    
    /**
     * Vraci funkci pro dany helper (podle nazvu)
     * @param string $method
     * @return callable
     */
    public function __invoke($method){
        foreach($this->helpers as $helper){
            if($helper->reflection->hasMethod($method)){
                return callback($helper, $method);
            }
        }
    }
    
    /**
     * Zaregistruje tridu s helpry
     * @param Object $helper 
     */
    public function register(Object $helper){
        $this->helpers[] = $helper;
    }
    
}