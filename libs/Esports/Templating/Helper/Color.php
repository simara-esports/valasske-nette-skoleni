<?php

namespace Esports\Templating\Helper;

/**
 * Nastavovani barvicek
 *
 * @author Svaťa
 */
class Color extends \Nette\Object {
    
    /**
     * Podle velikosti souboru vrati nazev barvy
     * @param int $size
     * @return string
     */
    public function bytesColor($size){
        if ($size > 10 * 1024 * 1024){
            return 'important';
        }
        if ($size > 1024 * 1024){
            return 'warning';
        }
        return 'info';
    }
    
    /**
     * Obarvi datum podle vnitrnich pravidel
     * @param int $timestamp
     * @param string $key
     * @return string
     */
    public function dateColor($timestamp, $key = 'important'){
        $settings = array(
            'important' => array(
                'important' => 'PT1H',
                'warning' => 'P1D',
                'success' => 'P7D',
            ),
            'success' => array(
		'success' => 'PT1H',
		'warning' => 'P1D',
		'important' => 'P1Y',
	    )
        );

        $date = \DateTime::createFromFormat('U', $timestamp);
        foreach($settings[$key] as $color => $diff){
            $now = now();
            $now->sub(new \DateInterval($diff));
            if($now < $date){
                return $color;
            } 
        }
        return 'info';
    }
    
}
