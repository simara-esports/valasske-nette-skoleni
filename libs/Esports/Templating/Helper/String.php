<?php

namespace Esports\Templating\Helper;

use \Nette\Utils\Strings;

/**
 * Helpery pro praci s pismenkama
 */
class String extends \Nette\Object{
    
    /**
     * Format data
     * @var string
     */
    protected $format = 'Y-m-d H:i:s';
    
    /**
     * Naformatuje datum podle globalni definice
     * @param \DateTime|mixed $date
     * @param string|null $format
     * @return string
     */
    public function date($date, $format = null){
        if(!$format){
            $format = $this->format;
        }
        if(is_int($date)){
            $date = \DateTime::createFromFormat('U', $date);
            $date->setTimeZone(new \DateTimeZone(date_default_timezone_get()));
        }
        
        if($date instanceof \DateTime){
            return $date->format($format);
        }
        return $date;
    }
    
    /**
     * Naformatuje hodnoty pro XML vystup
     * @param mixed $s
     * @return string 
     */
    public function xmlValue($s){
        if(is_bool($s)){
            return $s ? "true" : "false";
        }elseif(is_object($s)){
            if($s instanceof \DateTime){
                return $s->format('Y-m-d H:i:s');
            }
        }
        return $s;
    }
    
    /**
     * Z textove reprezentace data odstrani pripady cas
     * @param string $s
     * @return string
     */
    public function dateOnly($s){
        return Strings::replace($s, '~[ T][^: ]+:.+$~');
    }
    
    /**
     * Zformatuje byty na kB, MB, ..
     * @param int $size
     * @return string
     */
    public function bytes($size){
        $postfixes = array('', 'k', 'M', 'G', 'T', 'P', 'E');
        $radix = 1024;
        $postfixIndex = 0;
        do{
            $size /= $radix;
            $postfixIndex++;
        }while($size >= $radix);
        return number_format($size, 3, '.', ' ') . ' ' . $postfixes[$postfixIndex] . 'B';
    }
    
    /**
     * Prevod hodnoty do XML formatu
     * @param string $value
     * @return string
     */
    public function xml($value) {
        if(is_bool($value)){
            return $value ? "true" : "false";
        }
        return $value;
    }
}