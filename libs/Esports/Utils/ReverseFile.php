<?php

namespace Esports\Utils;

/**
 * Trida pro cteni souboru odzadu
 * @author Svaťa
 */
class ReverseFile{

    /**
     *
     * @var handle
     */
    protected $file;
    
    /**
     *
     * @var int
     */
    protected $position;

    public function __construct($filename){
	$this->file = fopen($filename, 'r');
	$this->position = -2;
    }

    /**
     * Vrati jeden radek textu
     * @return string
     */
    public function getLine(){
	$output = '';
	while(fseek($this->file, $this->position, SEEK_END) !== -1) {
	    $this->position--;
	    $char = fgetc($this->file);
	    if ($char === "\n") {
		    return $output;
		}
	    $output = $char . $output;
	}
    }
}