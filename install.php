<?php

/* 
 * Instalace
 * 
 * @author Svaťa
 */

foreach(array('temp', 'log') as $folder){
    @mkdir($folder);
    chmod($folder, 0777);
}

if(is_file('app/config/config.local.template.neon')){
    $config = file_get_contents('app/config/config.local.template.neon');
    file_put_contents('app/config/config.local.neon', $config);
}