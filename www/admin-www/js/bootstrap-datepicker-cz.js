$.fn.datepicker.dates['cz'] = {
    days: ['neděle', 'pondělí', 'úterý', 'středa', 'čtvrtek', 'pátek', 'sobota'],
    daysShort: ['ne', 'po', 'út', 'st', 'čt', 'pá', 'so'],
    daysMin: ['ne', 'po', 'út', 'st', 'čt', 'pá', 'so'],
    months: ['leden', 'únor', 'březen', 'duben', 'květen', 'červen', 'červenec', 'srpen',
        'září', 'říjen', 'listopad', 'prosinec'],
    monthsShort: ['led', 'úno', 'bře', 'dub', 'kvě', 'čer', 'čvc', 'srp', 'zář', 'říj', 'lis', 'pro'],
    today: "Nyní"
};