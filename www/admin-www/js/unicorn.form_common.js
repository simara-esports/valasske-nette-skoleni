/**
 * Unicorn Admin Template
 * Diablo9983 -> diablo9983@gmail.com
**/

$.unicornForms = function(){
    selector = $('input[type=checkbox]:not(.uniformed),input[type=radio]:not(.uniformed),input[type=file]:not(.uniformed)');
	selector.uniform();
    selector.toggleClass('uniformed');
	
	$('select').select2({ width: '80%' });
    $('.colorpicker').colorpicker();
    $('.datepicker').live('focus', function(){
        $(this).datepicker({
            language: 'cz',
            format: 'dd.mm.yyyy'
        });
    });
}

$(document).ready(function(){
	$.unicornForms();
});
