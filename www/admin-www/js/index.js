/*$(function () {
    $.nette.init();
    $.nette.ext('forms-redraw', { complete: $.unicornForms});
});*/

/**
 * Automaticke modalni okno objevujici se pri mazani polozky
 * Je aplikovano na odkazy, ktere maji atribut data-confirm-delete
 * Z tohoto atribudu se bere kontrolni otazka
 * @attr data-confirm-delete Kontrolni otazka
 * @attr data-confirm-submit Text potvrzovaciho tlacitka
 * @attr data-confirm-storno Text zamitaciho tlacitka
 */
$(function() {
	$('a[data-confirm-delete], input[data-confirm-delete]').live('click', function(ev) {
        self = $(this);
		var href = self.attr('href');
        var submitText = self.attr('data-confirm-submit') ? self.attr('data-confirm-submit') : 'Smazat';
        var stornoText = self.attr('data-confirm-storno') ? self.attr('data-confirm-storno') : 'Nic nemazat';
		if (!$('#dataConfirmModal').length) {
			$('body').append(
                '<div id="dataConfirmModal" class="modal" role="dialog"'
               +'aria-labelledby="dataConfirmLabel" aria-hidden="true">'
               +'<div class="modal-header">'
               +'<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>'
               +'<h3 id="dataConfirmLabel">Potvrzení</h3>'
               +'</div><div class="modal-body"></div><div class="modal-footer">'
               +'<button class="btn" data-dismiss="modal" aria-hidden="true">'
               + stornoText + '</button><a class="btn btn-danger" id="dataConfirmOK">'
               + submitText + '</a></div></div>');
		} 
		$('#dataConfirmModal').find('.modal-body').text(self.attr('data-confirm-delete'));
        if(self.is('a')){
            $('#dataConfirmOK').attr('href', href);
        }else if(self.is('input')){
            $('#dataConfirmOK').click(function(){
                self.closest('form').submit();
            });
        }
		$('#dataConfirmModal').modal({show:true});
		return false;
	});
});

//redactor
/*$(function(){
	$('.redactor').redactor({
        lang: 'cs',
        imageUpload: '/admin/upload/image',
        fileUpload: '/admin/upload/file',
        fixed: true
        //imageGetJson: '/admin/photo/jsonImages'
    }); 
});*/

$('input.change-submit, select.change-submit').live('change', function(e){
    $(this).closest('form').submit();
});

$('input.change-ajax-submit, select.change-ajax-submit').live('change', function(e){
    self = $(this);
    form = self.closest('form');
    form.netteAjax(e);
});

$('input.change-ajax-submit-click, select.change-ajax-submit-click').live('change', function(e){
    self = $(this);
    form = self.closest('form');
    form.find('input[type=submit]').click();
});

/*
 * Multicheckboxu prida "vse", pokud jsou vsechny prvky vybrane, vybere se taky,
 * zaroven prepina mezi vse/nic
 */
selectDeselect = function(self){
    id = self.attr('id') + '-all';
    type = id.match(/category/) ? 'category' : 'track';
    controls = self.find('.controls');
    
    first = $('<label for="'+id+'"\n\
class="checkbox inline"><input type="checkbox" name="'+type+'[all]"\n\
id="'+id+'" value="1" class="first"/>Všechny</label>');
    controls.prepend(first);
    
    
    changeCheckbox = function(checkbox, value){
        checkbox.attr('checked', value);
        span = checkbox.closest('span');
        if(value && !span.hasClass('checked')){
            span.addClass('checked');
        }
        if(!value && span.hasClass('checked')){
            span.removeClass('checked');
        }
    }
    
    inputs = controls.find('input:not(.first)');
    
    inputs.change(function(){
        firstCheckbox = self.find('.controls').find('#'+self.attr('id') + '-all');
        
        input = $(this);
        if(input.is(':checked')){
            allTrue = true;
            inputs.each(function(){
                if(!$(this).is(':checked'))
                   allTrue = false; 
            });
            if(allTrue){
                changeCheckbox(firstCheckbox, true);
            }
        }else{
            changeCheckbox(firstCheckbox, false);
        }
    });
    
    firstCheckbox = self.find('.controls').find('#'+self.attr('id') + '-all');
    
    firstCheckbox.change(function(){
        inputs = self.find('.controls').find('input:not(.first)');
        value = $(this).is(':checked');
        inputs.each(function(){
            changeCheckbox($(this), value);
        });
    });
}

selectDeselect($('#frm-registrationExportForm-form-track-pair'));
selectDeselect($('#frm-registrationExportForm-form-category-pair'));


$('.results-import').click(function(){
    $(this).find('input[type=file]').click();
});

$('.results-import input').click(function(e){
    e.stopPropagation();
});

// hromadne mazani registraci - zmena data v confirmation dialogu
changeMultiDeleteText = function(self){
    text = $('#registration-multidelete-submit').attr('data-confirm-delete');
    date = self.val().replace(/ /g, '');
    textWithDate = text.replace(/ [^ ]*$/, ' ' + date + '?');
    text = $('#registration-multidelete-submit').attr('data-confirm-delete', textWithDate);
}

$('#registration-multidelete-date').keyup(function(){
    changeMultiDeleteText($(this));
});

$(function(){
    dateInput = $('#registration-multidelete-date');
    if(dateInput.size())
        changeMultiDeleteText(dateInput);
});

var protectedForms = $('.protected-form');
protectedForms.find('input, select, textarea').attr('readonly', 'readonly');
protectedForms.find('input[type=radio]').attr('disabled', 'disabled');
protectedForms.append("<div class='form-actions'><button class='unlock btn'>Povolit úpravy</button></div>");

$('.unlock').live('click', function(){
    var self = $(this);
    self.closest('form').find('input:not(.force-readonly), select:not(.force-readonly), textarea:not(.force-readonly)')
            .attr('readonly', null);
    self.closest('form').find('input[type=radio]:not(.force-disabled)')
            .attr('disabled', null).closest('.disabled').toggleClass('disabled');
    self.closest('.form-actions').remove();
    self.remove();
    return false;
});

$('body').on('change', 'select', function(){
    var self = $(this);
    if(self.attr('data-change-ajax-link')){
        var link = self.attr('data-change-ajax-link');
        var value = self.val();
        link = link.replace('OPTION', value);
        $.nette.ajax(link);
    }
});