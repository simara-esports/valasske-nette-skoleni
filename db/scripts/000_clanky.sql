CREATE  TABLE IF NOT EXISTS `article` (
  `id` INT(10) NOT NULL AUTO_INCREMENT ,
  `id_lang` TINYINT(3) UNSIGNED NOT NULL DEFAULT 1 ,
  `date_time_create` DATETIME NULL DEFAULT NULL COMMENT 'datum kdy byl clanek vytvoren' ,
  `date_time_publish` DATETIME NULL DEFAULT NULL COMMENT 'datum kdy ma byt clanek automaticky zobrazen' ,
  `date_time_expire` DATETIME NULL DEFAULT NULL COMMENT 'datum kdy ma byt clanek automaticky vypnut' ,
  `date_time` DATETIME NULL DEFAULT NULL COMMENT 'zobrazene datum u clanku' ,
  `author` VARCHAR(80) NOT NULL DEFAULT 'Redakce' ,
  `is_visible` TINYINT(1) NOT NULL DEFAULT 0 ,
  `is_topstory` TINYINT(1) NOT NULL DEFAULT 0 ,
  `topstory_order` TINYINT(1) NOT NULL DEFAULT 0 COMMENT 'pozice v poradi vice topstory clanku' ,
  `viewed` INT(3) NOT NULL DEFAULT 0 COMMENT 'pocet precteni clanku' ,
  `header` VARCHAR(255) NULL DEFAULT NULL ,
  `sub_header` VARCHAR(255) NULL DEFAULT NULL ,
  `perex` TEXT NULL DEFAULT NULL ,
  `text` LONGTEXT NULL DEFAULT NULL ,
  `id_image_1` INT(10) NULL DEFAULT NULL ,
  `id_image_2` INT(10) NULL DEFAULT NULL ,
  `id_image_3` INT(10) NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `index2` (`id_lang` ASC) ,
  INDEX `index3` (`date_time` ASC) ,
  INDEX `index4` (`is_visible` ASC) ,
  INDEX `index5` (`is_topstory` ASC) )
ENGINE = InnoDB
COMMENT = 'clanky';

CREATE  TABLE IF NOT EXISTS `article_category` (
  `id` INT(10) NOT NULL AUTO_INCREMENT ,
  `id_lang` TINYINT(3) UNSIGNED NOT NULL DEFAULT 1 ,
  `is_visible` TINYINT(1) NOT NULL DEFAULT 0 ,
  `name` VARCHAR(255) NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
COMMENT = 'clanky - kategorie';

INSERT INTO article_category (id_lang, is_visible, name) VALUES
(1, 1, 'Cat1'),
(1, 1, 'Cat2'),
(1, 1, 'Cat3'),
(1, 1, 'Cat4'),
(2, 1, 'CatA'),
(2, 1, 'CatB'),
(2, 1, 'CatC'),
(2, 1, 'CatD'),
(1, 0, 'Cat nonvisible1'),
(1, 0, 'Cat nonvisible2'),
(1, 0, 'Cat nonvisible3'),
(2, 0, 'Cat nonvisibleA');




CREATE  TABLE IF NOT EXISTS `article_has_article_category` (
  `id` INT(10) NOT NULL AUTO_INCREMENT ,
  `id_article` INT(10) NOT NULL ,
  `id_article_category` INT(10) NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_article_has_category_article1_idx` (`id_article` ASC) ,
  INDEX `fk_article_has_category_category2_idx` (`id_article_category` ASC) ,
  CONSTRAINT `fk_article_has_category_article1`
    FOREIGN KEY (`id_article` )
    REFERENCES `article` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_article_has_category_category2`
    FOREIGN KEY (`id_article_category` )
    REFERENCES `article_category` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'vazba clanek - kategorie';